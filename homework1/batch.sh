#!/usr/bin
#SBATCH --job-name=MYJOB

### File for the output
#SBATCH --output=MYJOB_OUTPUT

### Time your job needs to execute, e. g. 15 min 30 sec
#SBATCH --time=00:15:30

### Memory your job needs per node, e. g. 1 GB
#SBATCH --mem=1G

### Execute your application
slurm.py
